import React from "react";

function NumberOfFans(props) {
  const formatFans = (nmber) => {
    let formattedNmber = nmber;
    if (nmber >= 1000) {
      var suffixes = ["", "k", "m", "b", "t"];
      var suffixNum = Math.floor(("" + nmber).length / 3);
      var shortValue = "";
      for (var precision = 2; precision >= 1; precision--) {
        shortValue = parseFloat(
          (suffixNum != 0
            ? nmber / Math.pow(1000, suffixNum)
            : value
          ).toPrecision(precision)
        );
        var dotLessShortValue = (shortValue + "").replace(
          /[^a-zA-Z 0-9]+/g,
          ""
        );
        if (dotLessShortValue.length <= 2) {
          break;
        }
      }
      if (shortValue % 1 != 0) shortValue = shortValue.toFixed(1);
      formattedNmber = shortValue + suffixes[suffixNum];
    }
    return formattedNmber;
  };

  return <>{formatFans(props.nb_fan)}</>;
}

export default NumberOfFans;
