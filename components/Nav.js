import Link from "next/link";
import Image from "next/image";
import SearchBar from "./SearchBar";

const Nav = () => (
  <nav>
    <ul>
      <li className="logo">
        <Link href="/">
          <img
            src="/images/deezer-logo.png"
            alt="Deezer logo"
            width="117"
            height="30"
          />
        </Link>
      </li>
      <li className="search-block">
        <SearchBar />
      </li>
    </ul>

    <style jsx>{`
      :global(body) {
        width: 100vw;
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
      }
      nav {
        width: 100vw;
        overflow: hidden;
        text-align: center;
        border-bottom: 1px solid #eaeaea;
        background-color: #ededed;
      }
      ul {
        display: flex;
        justify-content: flex-start;
        flex-direction: row;
      }
      nav > ul {
        padding: 32px 16px;
        margin-top: 0;
      }
      li {
        display: flex;
        padding: 6px 8px;
        flex-direction: row;
        justify-content: flex-start;
        align-items: flex-start;
      }
      .search-block {
        width: 100%;
      }
      a {
        color: #067df7;
        text-decoration: none;
        font-size: 13px;
      }
      @media screen and (max-width: 500px) {
        nav > ul {
          padding: 16px 0 0 0;
        }
        ul {
          flex-direction: column;
        }
        li {
          justify-content: center;
          align-items: center;
        }
        .logo img {
          width: 160px;
          height: 41px;
        }
      }
    `}</style>
  </nav>
);

export default Nav;
