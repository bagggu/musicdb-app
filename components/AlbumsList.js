import React, { useContext, useEffect, useState } from "react";
import superagent from "superagent";
import AlbumCard from "./AlbumCard";
import PageHeading from "./PageHeading";
import { TracksSelectedContext } from "../context/TracksSelectedContext";
import { AppContext } from "../context/AppContext";

function AlbumsList(props) {
  const [tracksSelected] = useContext(TracksSelectedContext);
  const [status, setStatus] = useContext(AppContext);
  const [albums, setAlbums] = useState([]);

  useEffect(() => {
    async function siftAlbums() {
      setStatus({ ...status, loading: true });
      if (tracksSelected.length > 0) {
        let albumsList = [];
        await tracksSelected.map((track, index) => {
          // limit list to 6 albums to avoid reaching 429 (Too Many Requests) so quickly
          if (albumsList.length < 6) {
            const found = albumsList.find(
              (album) => album.id === track.album.id
            );
            if (!found) {
              // seek album release year
              const proxyurl = "https://cors-anywhere.herokuapp.com/";
              const url = `https://api.deezer.com/album/${track.album.id}`;

              superagent.get(proxyurl + url).end((err, res) => {
                err && console.log("error", err);
                if (res) {
                  if (res.body && res.body.release_date) {
                    let release_year = res.body.release_date.split("-")[0];
                    albumsList.push({
                      ...track.album,
                      albumYear: release_year,
                    });
                  }
                }
              });
            }
          }
        });

        setAlbums(albumsList);
      }
      setStatus({ ...status, loading: false });
    }
    siftAlbums();
  }, [tracksSelected]);

  useEffect(() => {
    return () => {
      setAlbums([]);
    };
  }, []);

  return (
    <>
      {status.loading ? (
        <div>Loading</div>
      ) : !status.loading && albums.length > 0 ? (
        <>
          <PageHeading title={"Albums"} />
          <div className="album-list">
            {albums.map((album, index) => (
              <AlbumCard
                key={`album-${index}`}
                borderRadius={"8px"}
                album={album}
              />
            ))}

            <style jsx>{`
              .album-list {
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                margin-left: 34px;
              }
            `}</style>
          </div>
        </>
      ) : (
        <PageHeading title={"No albums available"} />
      )}
    </>
  );
}

export default AlbumsList;
