import React, { useEffect } from "react";
import Image from "next/image";

function AlbumCard(props) {
  const { album } = props;

  return (
    <div className="album-card">
      <div className="album-image">
        <Image
          src={album.cover_medium}
          alt={album.title}
          layout="responsive"
          objectFit="cover"
          quality={75}
          width={150}
          height={150}
        />
      </div>
      <div className="album-title">{album.title}</div>
      <div className="album-year">{album.releaseYear}</div>

      <style jsx>{`
        .album-card {
          width: 150px;
          display: flex;
          justify-content: flex-start;
          align-items: flex-start;
          flex-direction: column;
          color: #52525d;
          padding: 15px 0;
        }
        .album-image {
          width: 100%;
          overflow: hidden;
          -webkit-box-shadow: 0 1px 6px rgba(25, 25, 34, 0.18);
          box-shadow: 0 1px 6px rgba(25, 25, 34, 0.18);
        }
        .album-title {
          font-size: 12px;
          font-weight: 600;
          color: #000;
          margin-top: 12px;
          margin-bottom: 4px;
          text-decoration: none;
        }
        .album-year {
          font-size: 12px;
          margin-bottom: 8px;
        }
        a {
          color: #52525d;
          text-decoration: none;
        }
        a:hover {
          text-decoration: underline;
        }

        @media screen and (max-width: 500px) {
          .artist-card {
            margin: auto;
          }
        }
      `}</style>
    </div>
  );
}

export default AlbumCard;
