import React, { useContext, useRef } from "react";
import superagent from "superagent";
import Router, { useRouter } from "next/router";
import { ArtistContext } from "../context/ArtistContext";
import { AppContext } from "../context/AppContext";

const SearchBar = (props) => {
  const [status, setStatus] = useContext(AppContext);
  const [artists, setArtists] = useContext(ArtistContext);

  const searchText = useRef(null);

  const { route } = useRouter();

  // useEffect(() => {
  // Load default content when one loads the page
  // if (route !== "/artist/[id]" && artists.length === 0 && !status.loading) {
  // processSearch("Africa");
  // }
  // }, []);

  const processSearch = (q) => {
    if (q !== "") {
      setArtists([]);
      setStatus({ ...status, loading: true });
      const proxyurl = "https://cors-anywhere.herokuapp.com/";
      const url = `https://api.deezer.com/search/artist?q=${q}`;

      superagent.get(proxyurl + url).end((err, res) => {
        err && console.log("error", err);
        if (res.body) {
          setArtists(res.body.data);
        }
      });
      setStatus({ ...status, loading: false });
      if (route === "/artist/[id]") {
        Router.push("/");
      }
    }
  };

  const searchArtist = () => {
    processSearch(searchText.current.value);
  };

  const handleKey = (e) => {
    if (e.charCode == 13) {
      processSearch(searchText.current.value);
    }
  };

  return (
    <div className="search-bar">
      <div className="search-input">
        <input
          id="search-textbox"
          placeholder="Search for great artists"
          ref={searchText}
          onKeyPress={(e) => handleKey(e)}
        />
      </div>
      <div className="search-icon" onClick={searchArtist}>
        <img
          src="/images/search-icon.svg"
          alt="search"
          width="22"
          height="22"
        />
      </div>
      <div className="loading">
        {status.loading ? (
          <img src="/images/bars.svg" alt="loading" height="22" />
        ) : (
          ""
        )}
      </div>

      <style jsx>{`
        .search-bar {
          margin-left: 34px;
          width: 100%;
          display: flex;
          justify-content: flex-start;
          align-items: center;
          flex-direction: row;
          color: #52525d;
        }
        .search-icon {
          border-radius: ${props.borderRadius};
          overflow: hidden;
          cursor: pointer;
          opacity: 0.4;
        }
        .search-input {
          width: 400px;
        }
        #search-textbox {
          background-color: #fff;
          border: none;
          color: #52525d;
          -webkit-flex: 1;
          -ms-flex: 1;
          flex: 1;
          font-size: 16px;
          height: 32px;
          padding-left: 14px;
          padding-right: 14px;
          width: 100%;
          margin-left: 20px;
        }
        #search-textbox:focus {
          outline: 4px solid #fff;
        }
        .search-icon:hover {
          opacity: 1;
        }
        .loading {
          width: 200px;
        }
        @media screen and (max-width: 500px) {
          .search-bar {
            width: 100vw;
            display: flex;
            justify-content: flex-start;
            align-items: center;
            flex-direction: column;
            margin-left: 0;
          }
          #search-textbox {
            font-size: 18px;
            height: 42px;
            margin-top: 16px;
            margin-left: 0;
          }
          .search-input {
            width: 70vw;
            margin-left: 0;
            overflow: hidden;
          }
          .search-block {
            margin-top: 20px;
          }
          .search-icon {
            display: none;
          }
          .loading {
            margin-top: 20px;
          }
        }
      `}</style>
    </div>
  );
};

export default SearchBar;
