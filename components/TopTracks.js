import React, { useContext, useEffect } from "react";
import superagent from "superagent";
import { AppContext } from "../context/AppContext";
import { TracksSelectedContext } from "../context/TracksSelectedContext";

function TopTracks(props) {
  const [status, setStatus] = useContext(AppContext);
  const [tracksSelected, setTracksSelected] = useContext(TracksSelectedContext);

  const retrieveTracks = (query) => {
    setStatus({ ...status, loading: true });
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = query;

    superagent.get(proxyurl + url).end((err, res) => {
      err && console.log("error", err);
      if (res.body) {
        // order by rank since this is a top tracks list
        // seems unnecessary. It's like Deezer already returns the objects in rank order
        // let unorderedTracks = res.body.data;
        // console.log("pre", unorderedTracks);
        // unorderedTracks.sort(function (a, b) {
        //   return a.rank - b.rank;
        // });
        // console.log("post", unorderedTracks);

        setTracksSelected(res.body.data);
      }
    });
    setStatus({ ...status, loading: false });
  };

  useEffect(() => {
    retrieveTracks(props.tracklist);
  }, []);

  useEffect(() => {
    return () => {
      setTracksSelected({});
    };
  }, []);

  const timeWrangler = (time) => {
    const minutes = Math.floor(time / 60);
    const seconds = time - minutes * 60;
    const hours = Math.floor(time / 3600);
    function padding(string, pad, length) {
      return (new Array(length + 1).join(pad) + string).slice(-length);
    }
    let formattedTime;
    if (hours) {
      formattedTime =
        padding(hours, "0", 2) +
        ":" +
        padding(minutes, "0", 2) +
        ":" +
        padding(seconds, "0", 2);
    } else {
      formattedTime = padding(minutes, "0", 2) + ":" + padding(seconds, "0", 2);
    }
    return formattedTime;
  };

  return (
    <div className="top-tracks" style={{ width: "100%", padding: "12px" }}>
      {status.loading ? (
        <img src="/images/bars.svg" alt="loading" height="22" />
      ) : !status.loading && tracksSelected.length === 0 ? (
        <div> No tracks available</div>
      ) : !status.loading && tracksSelected.length > 0 ? (
        <>
          <h3>Top tracks</h3>
          <div className="track-list">
            {!status.loading &&
              tracksSelected.map(
                (track, index) =>
                  index < 6 && (
                    <div key={index} className="track-detail">
                      <div className="track-name">
                        <strong>{index + 1}.</strong> {track.title}
                      </div>
                      <div>{timeWrangler(track.duration)}</div>
                    </div>
                  )
              )}

            <style jsx>{`
              .top-tracks {
                display: flex;
                flex-direction: column;
                padding: 8px;
                width: 100%;
              }
              .track-list {
                display: flex;
                flex-direction: column;
                flex-wrap: wrap;
                width: 100%;
              }
              .track-detail {
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                align-items: center;
                color: #52525d;
                border-bottom: 2px solid #ededed;
                padding: 5px 0;
                font-size: 0.9em;
                width: 100%;
              }
              .track-name {
                max-width: 70%;
              }
            `}</style>
          </div>
        </>
      ) : null}
    </div>
  );
}

export default TopTracks;
