import React, { useContext, useEffect } from "react";
import superagent from "superagent";
import { isMobile } from "react-device-detect";
import { ArtistSelectedContext } from "../context/ArtistSelectedContext";
import AlbumsList from "./AlbumsList";
import ArtistHeroDesktop from "./ArtistHeroDesktop";
import ArtistHeroMobile from "./ArtistHeroMobile";

function ArtistDetail(props) {
  const [artistSelected, setArtistSelected] = useContext(ArtistSelectedContext);

  const processSearch = () => {
    if (props.id !== "") {
      console.log("start serach 2", props.id);

      setStatus({ ...status, loading: true });
      const proxyurl = "https://cors-anywhere.herokuapp.com/";
      const url = `https://api.deezer.com/artist/${props.id}`;
      console.log(url);

      superagent.get(proxyurl + url).end((err, res) => {
        err && console.log("error", err);
        if (res) {
          setArtistSelected(res.body.data);
        }
      });
      setStatus({ ...status, loading: false });
    }
  };

  useEffect(() => {
    console.log(isMobile);
    if (artistSelected.length === 0 && props.id !== "") {
      processSearch();
    }
  }, []);

  return (
    <>
      {isMobile ? (
        <ArtistHeroMobile artist={artistSelected} />
      ) : (
        <ArtistHeroDesktop artist={artistSelected} />
      )}
      <AlbumsList />
    </>
  );
}

export default ArtistDetail;
