import React, { useContext } from "react";
import ArtistCard from "./ArtistCard";
import PageHeading from "./PageHeading";
import { ArtistContext } from "../context/ArtistContext";
import { AppContext } from "../context/AppContext";

function ArtistsList() {
  const [artists] = useContext(ArtistContext);
  const [status] = useContext(AppContext);

  return (
    <>
      <PageHeading
        title={
          artists.length > 0
            ? "Artists"
            : artists.length === 0 && !status.loading
            ? "No listed artists"
            : "Find some great artists"
        }
      />
      <div className="artists-list">
        {!status.loading &&
          artists.map((artist, index) => (
            <ArtistCard borderRadius={"8px"} artist={artist} key={index} />
          ))}

        <style jsx>{`
          .artists-list {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
          }
          ul {
            display: flex;
            justify-content: space-between;
          }
          nav > ul {
            padding: 4px 16px;
          }
          li {
            display: flex;
            padding: 6px 8px;
          }
          a {
            color: #067df7;
            text-decoration: none;
            font-size: 13px;
          }
        `}</style>
      </div>
    </>
  );
}

export default ArtistsList;
