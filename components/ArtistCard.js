import React, { useContext } from "react";
import Router from "next/router";
import Image from "next/image";
import { ArtistSelectedContext } from "../context/ArtistSelectedContext";
import NumberOfFans from "../components/NumberOfFans";

function ArtistCard(props) {
  const [artistSelected, setArtistSelected] = useContext(ArtistSelectedContext);

  const { artist } = props;

  const selectArtist = () => {
    setArtistSelected(artist);
    Router.push(`/artist/${artist.id}`);
  };

  return (
    <div className="artist-card" onClick={selectArtist}>
      <div className="artist-image">
        <Image
          src={artist.picture_medium}
          alt={artist.name}
          layout="responsive"
          objectFit="cover"
          quality={75}
          width={268}
          height={268}
        />
      </div>
      <div className="artist-name">{artist.name}</div>
      <div className="artist-fans">
        <NumberOfFans nb_fan={artist.nb_fan} /> Fans
      </div>

      <style jsx>{`
        .artist-card {
          margin-left: 34px;
          width: 264px;
          display: flex;
          justify-content: space-between;
          align-items: flex-start;
          flex-direction: column;
          color: #52525d;
          padding: 15px 0;
          cursor: pointer;
        }
        .artist-image {
          width: 100%;
          border-radius: ${props.borderRadius};
          overflow: hidden;
          -webkit-box-shadow: 0 1px 6px rgba(25, 25, 34, 0.18);
          box-shadow: 0 1px 6px rgba(25, 25, 34, 0.18);
        }
        .artist-name {
          font-size: 15px;
          font-weight: 600;
          color: #000;
          margin-top: 12px;
          margin-bottom: 8px;
          text-decoration: none;
        }
        .artist-fans {
          font-size: 12px;
          margin-bottom: 8px;
        }
        a {
          color: #52525d;
          text-decoration: none;
        }
        a:hover {
          text-decoration: underline;
        }

        @media screen and (max-width: 500px) {
          .artist-card {
            margin: auto;
          }
        }
      `}</style>
    </div>
  );
}

export default ArtistCard;
