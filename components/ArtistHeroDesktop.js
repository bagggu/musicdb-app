import React, { useContext } from "react";
import { AppContext } from "../context/AppContext";
import TopTracks from "../components/TopTracks";
import NumberOfFans from "../components/NumberOfFans";

function ArtistHeroDesktop(props) {
  const { artist } = props;

  return (
    <>
      <div className="artist-wrapper">
        <div className="image-gradient-layer"></div>
        <div className="artist-hero">
          <div className="artist-left-block">
            <div className="artist-details">
              <h3>{artist.name}</h3>
              {artist.nb_fan && (
                <div className="artist-fans">
                  <strong>
                    <NumberOfFans nb_fan={artist.nb_fan} />{" "}
                  </strong>
                  Fans
                </div>
              )}
            </div>
            <div className="artist-image-thumb">
              <img
                src={artist.picture_medium}
                alt={artist.name}
                width="100"
                height="100"
              />
            </div>
          </div>
        </div>
        <div className="artist-right-block">
          <TopTracks tracklist={artist.tracklist} />
        </div>
      </div>

      <style jsx>{`
        .artist-wrapper {
          width: 100vw;
          display: flex;
          flex-direction: row;
          align-items: center;
          justify-content: space-between;
          position: relative;
        }
        .artist-hero {
          display: flex;
          flex-direction: row;
          padding-left: 30px;
          align-items: center;
          justify-content: flex-start;
          margin: 24px 0 24px 24px;
          background-image: url(${artist.picture_xl});
          background-size: 50000px 50000px;
          height: 300px;
          flex: 2;
        }
        .artist-hero h3 {
          font-size: 1.8em;
        }
        .artist-fans {
          font-size: 1.1em;
          border-bottom: 2px solid #fff;
        }
        .artist-left-block {
          display: flex;
          flex: 2;
        }
        .artist-right-block {
          display: flex;
          flex: 1;
          background-color: #fff;
          height: 300px;
        }
        .artist-details {
          display: flex;
          flex: 3;
          flex-direction: column;
          justify-content: center;
          align-items: flex-start;
          color: #fff;
          position: relative;
        }
        .image-gradient-layer {
          position: absolute;
          background: linear-gradient(90deg, rgba(0, 0, 0, 0.55), transparent);
          width: 60.2vw;
          left: 24px;
          height: 240px;
          padding: 30px;
        }
        .artist-image-thumb {
          display: flex;
          flex: 1;
          flex-direction: row;
          justify-content: center;
          align-items: flex-start;
          width: 100px;
          height: 100px;
          overflow: hidden;
          z-index: 9;
        }
        .artist-image-thumb img {
          -webkit-box-shadow: 0 1px 6px rgba(25, 25, 34, 0.18);
          box-shadow: 0 1px 6px rgba(25, 25, 34, 0.18);
        }

        @media screen and (max-width: 500px) {
          .page-heading {
            justify-content: center;
            padding-left: 0;
            margin: 9px 0 9px 0;
          }
        }
      `}</style>
    </>
  );
}

export default ArtistHeroDesktop;
