import React, { useContext } from "react";
import { AppContext } from "../context/AppContext";
import TopTracks from "../components/TopTracks";
import NumberOfFans from "../components/NumberOfFans";

function ArtistHeroMobile(props) {
  const { artist } = props;

  return (
    <>
      <div className="artist-image-thumb">
        <img
          src={artist.picture_medium}
          alt={artist.name}
          width="264"
          height="264"
        />
      </div>
      <div className="artist-details">
        <h3>{artist.name}</h3>
        {artist.nb_fan && (
          <div className="artist-fans">
            <strong>
              <NumberOfFans nb_fan={artist.nb_fan} />{" "}
            </strong>
            Fans
          </div>
        )}
      </div>

      <div className="tracks-block">
        <TopTracks tracklist={artist.tracklist} />
      </div>

      <style jsx>{`
        .tracks-block {
          width: 80vw;
          padding: 12px;
          -webkit-box-shadow: 0 1px 6px rgba(25, 25, 34, 0.18);
          box-shadow: 0 1px 6px rgba(25, 25, 34, 0.18);
          margin: auto;
        }
        .artist-wrapper {
          width: 100vw;
          display: flex;
          flex-direction: row;
          align-items: center;
          justify-content: space-between;
          position: relative;
        }

        .artist-fans {
          font-size: 1.1em;
          border-bottom: 2px solid #5b5b5b;
        }

        .artist-right-block {
          display: flex;
          flex: 1;
          background-color: #fff;
          height: 300px;
        }
        .artist-details {
          display: flex;
          flex-direction: column;
          justify-content: flex-start;
          align-items: flex-start;
          margin-left: 10vw;
          margin-bottom: 24px;
        }

        .artist-image-thumb {
          padding-top: 20px;
          display: flex;
          flex: 1;
          flex-direction: row;
          justify-content: center;
          align-items: flex-start;
          width: 264px;
          height: 264px;
          overflow: hidden;
          z-index: 9;
          margin: auto;
        }
        .artist-image-thumb img {
          -webkit-box-shadow: 0 1px 6px rgba(25, 25, 34, 0.18);
          box-shadow: 0 1px 6px rgba(25, 25, 34, 0.18);
        }

        @media screen and (max-width: 500px) {
          .page-heading {
            justify-content: center;
            padding-left: 0;
            margin: 9px 0 9px 0;
          }
        }
      `}</style>
    </>
  );
}

export default ArtistHeroMobile;
