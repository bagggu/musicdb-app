import React, { useEffect, useState, useContext, useRef } from "react";
import { AppContext } from "../context/AppContext";
import { ArtistContext } from "../context/ArtistContext";

function PageHeading(props) {
  const [status, setStatus] = useContext(AppContext);
  const [artists, setArtists] = useContext(ArtistContext);

  return (
    <div className="page-heading">
      {!status.loading && artists.length > 0 && (
        <>
          <h3>{props.title}</h3>
          <div className="right-caret">
            <img
              src="/images/right-caret.svg"
              alt="right caret"
              width="13"
              height="22"
            />
          </div>
        </>
      )}

      <style jsx>{`
        .page-heading {
          display: flex;
          flex-direction: row;
          padding-left: 34px;
          align-items: center;
          margin: 24px 0 9px 0;
          justify-content: flex-start;
        }
        .right-caret {
          margin-left: 6px;
          padding-top: 8px;
        }

        @media screen and (max-width: 500px) {
          .page-heading {
            justify-content: center;
            padding-left: 0;
            margin: 9px 0 9px 0;
          }
        }
      `}</style>
    </div>
  );
}

export default PageHeading;
