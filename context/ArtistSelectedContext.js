import React, { useState, createContext } from "react";

export const ArtistSelectedContext = createContext();

export const ArtistSelectedContextProvider = (props) => {
  const [artistSelected, setArtistSelected] = useState({});

  return (
    <ArtistSelectedContext.Provider value={[artistSelected, setArtistSelected]}>
      {props.children}
    </ArtistSelectedContext.Provider>
  );
};
