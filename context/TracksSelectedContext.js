import React, { useState, createContext } from "react";

export const TracksSelectedContext = createContext();

export const TracksSelectedContextProvider = (props) => {
  const [tracksSelected, setTracksSelected] = useState([]);

  return (
    <TracksSelectedContext.Provider value={[tracksSelected, setTracksSelected]}>
      {props.children}
    </TracksSelectedContext.Provider>
  );
};
