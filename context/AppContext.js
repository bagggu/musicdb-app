import React, { useState, createContext } from "react";

export const AppContext = createContext();

export const AppContextProvider = (props) => {
  const [status, setStatus] = useState({ loading: false });

  return (
    <AppContext.Provider value={[status, setStatus]}>
      {props.children}
    </AppContext.Provider>
  );
};
