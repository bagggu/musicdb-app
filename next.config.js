module.exports = {
  webpack: (config) => {
    config.node = {
      fs: "empty",
    };
    return config;
  },
  images: {
    domains: ["e-cdns-images.dzcdn.net", "cdns-images.dzcdn.net"],
  },
};
