import React, { useEffect } from "react";
import Router, { useRouter } from "next/router";
import Head from "../../components/Head";
import Nav from "../../components/Nav";
import ArtistDetail from "../../components/ArtistDetail";

function Artist() {
  const { id } = useRouter();

  useEffect(() => {}, []);
  return (
    <>
      <Head title="Deezer - music streaming | Try Flow, download &amp; listen to free music" />
      <Nav />
      <ArtistDetail id={id} />
    </>
  );
}

export default Artist;
