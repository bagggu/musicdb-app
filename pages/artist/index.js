import React, { useEffect } from "react";
import Router from "next/router";
import Head from "../../components/Head";
import Nav from "../../components/Nav";

function ArtistIndex() {
  useEffect(() => {
    // redirect to landing page if no artist id is specified in url
    Router.push("/");
  }, []);
  return (
    <>
      <Head title="Deezer - music streaming | Try Flow, download &amp; listen to free music" />
      <Nav />
    </>
  );
}

export default ArtistIndex;
