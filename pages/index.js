import Head from "../components/Head";
import Nav from "../components/Nav";
import ArtistsList from "../components/ArtistsList";

function Index() {
  return (
    <>
      <Head title="Deezer - music streaming | Try Flow, download &amp; listen to free music" />
      <Nav />
      <ArtistsList />
    </>
  );
}

export default Index;
