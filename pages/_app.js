import { AppContextProvider } from "../context/AppContext";
import { ArtistContextProvider } from "../context/ArtistContext";
import { ArtistSelectedContextProvider } from "../context/ArtistSelectedContext";
import { TracksSelectedContextProvider } from "../context/TracksSelectedContext";

function ArtistApp({ Component, pageProps }) {
  return (
    <AppContextProvider>
      <ArtistContextProvider>
        <ArtistSelectedContextProvider>
          <TracksSelectedContextProvider>
            <Component {...pageProps} />
          </TracksSelectedContextProvider>
        </ArtistSelectedContextProvider>
      </ArtistContextProvider>
    </AppContextProvider>
  );
}

export default ArtistApp;
